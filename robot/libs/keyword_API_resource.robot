*** Settings ***
Library           RequestsLibrary
Library           FakerLibrary
Library           String
Library           Collections
Library           JSONLibrary
Library           SeleniumLibrary    timeout=7 seconds
Library           OperatingSystem

*** Variables ***
### API Variables ###
# ${HOST}         https://dev.sideways.eq
${HOST}           https://dev.sideways.work
${HOST_GETNADA}    https://getnada.com/api/v1/inboxes
${HOST_GETNADA_MESSAGE}    https://getnada.com/api/v1/messages/html

*** Keywords ***
Global API Variables
    ${firstNameData}    FakerLibrary.First Name
    ${lastNameData}    FakerLibrary.Last Name
    ${userNameData}    Set Variable    ${firstNameData}${lastNameData}
    ${randomNumb}    FakerLibrary.Random Number    4    fix_len=True
    ${newEmail}    Set Variable    milan.novovic+${randomNumb}@equaleyes.com
    ${knowledgeTitle}    FakerLibrary.Sentence    4
    ${newUsername}=    Set Variable    ${userNameData}${randomNumb}
    ${UserLowercase}    Convert To Lower Case    ${userNameData}
    #login user
    ${login_string}=    catenate
    ...    {
    ...    "password": "Testingqa123",
    ...    "username": "Milan",
    ...    "email": "milan.novovic@equaleyes.com"
    ...    }
    #create user
    ${create_string}=    catenate
    ...    {
    ...    "firstName": "${firstNameData}",
    ...    "lastName": "${lastNameData}",
    ...    "username": "${newUsername}",
    ...    "email":    "${UserLowercase}@givmail.com",
    ...    "password1": "Testingqa123",
    ...    "password2": "Testingqa123",
    ...    "organisation": "culpa voluptate",
    ...    "subdomain": "${userNameData}"
    ...    }
    Set Global Variable    ${firstNameData}
    Set Global Variable    ${lastNameData}
    Set Global Variable    ${login_string}
    Set Global Variable    ${create_string}
    Set Global Variable    ${userNameData}
    Set Global Variable    ${UserLowercase}
    Set Global Variable    ${newUsername}
    Log    ${newUsername}

User Send Login POST Request
    ${headers}=    Create Dictionary    Content-Type=application/json
    ${response}=    POST On Session    USER_LOGIN    /users/login/    data=${login_string}    headers=${headers}
    Set Global Variable    ${response}
    ${respHeaders}    Set Variable    ${response.headers}
    ${respJson}    Set Variable    ${response.json()}
    #Log Date and Token from responses
    ${valueDate}    Get Value From Json    ${respHeaders}    $..Date
    ${TokenDic}    Get Value From Json    ${respJson}    $.token
    ${valueToken}    Set Variable    ${TokenDic[0]}
    Log    ${valueDate}
    Log    ${valueToken}
    Set Global Variable    ${valueToken}

Response Status Is 200 OK
    Status Should Be    200    ${response}

User Send Registration POST Request
    ${headers}    Create Dictionary    Content-Type=application/json
    ${response}    POST On Session    USER_REGISTRATION    /users/registration/    data=${create_string}    headers=${headers}
    Set Global Variable    ${response}
    #Log Headers and JSON
    Log    ${response.headers}
    Log    ${response.json()}

Response Status is 201 OK
    Status Should Be    201    ${response}

User Gets Email Content
    Sleep    5s
    ${lowercase}    Convert To Lower Case    ${userNameData}
    Set Global Variable    ${lowercase}
    Log    ${userNameData}
    ${headers}    Create Dictionary    Content-Type=application/json
    ${response}    GET On Session    GETNADA_EMAIL    ${UserLowercase}@givmail.com    headers=${headers}
    Set Global Variable    ${response}
    #Log Headers and JSON
    Log    ${response.headers}
    Log    ${response.json()}
    ${respJson}    Set Variable    ${response.json()}
    ${uidMessage}    Get Value From Json    ${respJson}    $..uid
    ${uidValue}    Set Variable    ${uidMessage[0]}
    Set Global Variable    ${uidValue}
    Log    ${uidMessage[0]}

User Gets Registration Link
    Sleep    5s
    ${headers}=    Create Dictionary    Content-Type=application/json
    ${response}=    GET On Session    GETNADA_MESSAGE    ${uidValue}    headers=${headers}
    #Log Headers and JSON
    Log    ${response.content}
    ${activateBytes}    Set Variable    ${response.content}
    ${activateHTML}    Decode Bytes To String    ${activateBytes}    UTF-8
    ${match1}=    Get Regexp Matches    ${activateHTML}    https:.*?\<
    ${activateLink}=    Evaluate    "/".join($match1)
    ${activateLink}=    Remove String    ${activateLink}    <
    Log    ${activateLink}
    Set Global Variable    ${activateLink}

Is Possible To Login With Created Email
    Open Browser    ${activateLink}    ${BROWSER}    remote_url=${GRID_URL}
    #remote_url=${GRID_URL}
    Set Window Size    1920    1080
    Sleep    1
    Input Text    ${username}    ${newUsername}
    Input Text    ${password}    Testingqa123
    Click Element    ${SignIn}
    Sleep    2
    Input Text    //*[@id='registrationOrganisationName']    culpa voluptate
    Input Text    //*[@id='registrationOrganisationSubdomain']    ${userNameData}
    Sleep    2
    Click Element    //*[@id='registrationOrganisationNextStep']
    Wait Until Element Is Visible    //span[contains(@class,'mat-button-wrapper')][contains(text(),'Jump in')]
    Click Element    //span[contains(@class,'mat-button-wrapper')][contains(text(),'Jump in')]
    Sleep    2
    Page Should Contain Element    //span[contains(@class,'name')][contains(text(),'${firstNameData}')]
    Log    ${firstNameData}

User Can Create Content
    Log    ${valueToken}
    ${body}=    Create Dictionary    name=New Text Content    status=published
    ${headers}=    Create Dictionary    Authorization=Token ${valueToken}
    Sleep    3s
    ${response}=    POST On Session    CREATE_CONTENT    /content/    headers=${headers}    json=${body}
    Log    ${response.json()}
    ${contentID}    Set Variable    ${response.json()}
    ${justID}=    Get Value From Json    ${contentID}    $.id
    ${createdId}    Set Variable    ${justID[0]}
    Set Global Variable    ${createdId}
    Log    ${createdId}

User Can Upload Text
    ${json_string}=    catenate
    ...    {
    ...    "text": "Testing String",
    ...    "id":    0
    ...    }
    ${headers}=    Create Dictionary    Content-Type=application/json    Authorization=Token ${valueToken}
    ${response}=    POST On Session    UPLOAD_TEXT    /content/${createdId}/text/    data=${json_string}    headers=${headers}
    Log    ${response.status_code}
    Log    ${response.json()}

User Can Upload Image
    File Should exist    ${CURDIR}/../data/cover.jpg
    ${file_name}=    Get File For Streaming Upload    ${CURDIR}/../data/cover.jpg
    ${asset}=    Create Dictionary    image=${file_name}
    ${header}=    Create Dictionary    Authorization=Token ${valueToken}
    ${response}=    POST On Session    UPLOAD_IMAGE    /content/${createdId}/images/    files=${asset}    headers=${header}
    Log    ${response.status_code}

User Can Delete Image
    Sleep    5
    ${header}=    Create Dictionary    Authorization=Token ${valueToken}
    ${response}=    DELETE On Session    DELETE_IMAGE    /content/${createdId}/    headers=${header}
    Log    ${response.status_code}

Get Content List And Delete All Content
    ${headers}    Create Dictionary    Authorization=Token ${valueToken}
    ${response}    GET On Session    GET_CONTENT    /content/    headers=${headers}
    Sleep    3s
    Log    ${response.json()}
    @{returnGet}    Set variable    ${response.json()}
    ${images}    Create List
    FOR    ${item}    IN    @{returnGet}
        Append To List    ${images}    ${item['id']}
        Log    ${images}
    END
    FOR    ${imageId}    IN    @{images}
        Set Test Variable    ${imageId}
        Run Keyword And Ignore Error    Then User Can Delete Image In Sequence
        Log    ${response.json()}
    END

User Can Delete Image In Sequence
    Create Session    DELETE_IMAGE    ${HOST}/api    verify=True
    Sleep    3
    ${header}    Create Dictionary    Authorization=Token ${valueToken}
    ${response}    DELETE On Session    DELETE_IMAGE    /content/${imageId}/    headers=${header}

User Can Upload Image via API
    Create Session    USER_LOGIN    ${HOST}/api    verify=True
    And User Send Login POST Request
    Create Session    CREATE_CONTENT    ${HOST}/api    verify=True
    Then User Can Create Content
    Create Session    UPLOAD_IMAGE    ${HOST}/api    verify=True
    Then User Can Upload Image
    File Should Exist    ${CURDIR}/../data/cover.jpg
    ${file_name}    Get File For Streaming Upload    ${CURDIR}/../data/cover.jpg
    ${asset}    Create Dictionary    image=${file_name}
    ${header}    Create Dictionary    Authorization=Token ${valueToken}
    ${response}    POST On Session    UPLOAD_IMAGE    /content/${createdId}/images/    files=${asset}    headers=${header}
    Log    ${response.status_code}
