*** Settings ***
Library           String
Library           OperatingSystem
Library           SeleniumLibrary
Library           FakerLibrary
Resource          ../libs/keyword_API_resource.robot

*** Variables ***
 ### System Variables ###
${GRID_URL}       http://selenium:4444/wd/hub
${BROWSER}        Chrome
 ### Sign In ###
${username}       //input[@formcontrolname='username']
${password}       //input[@formcontrolname='password']
${SignIn}         //span[@class='mat-button-wrapper']
${validUsername}    milan.novovic@equaleyes.com
${validPassword}    Testingqa123
 ### Sign Up ###
${signUp}         //*[contains(text(),'Create organisation')]
${orgName}        //input[@formcontrolname='organisation']
${subDomain}      //input[@formcontrolname='subdomain']
${nextStep}       //span[normalize-space()='Next step']
${userName}       //input[@formcontrolname='username']
${firstName}      //input[@formcontrolname='firstName']
${lastName}       //input[@formcontrolname='lastName']
${signEmail}      //input[@formcontrolname='email']
${signPassword}    //input[@formcontrolname='password']
${repeatPassword}    //input[@formcontrolname='repeatPassword']
${signPassword}    TestingQA123
${checkboxAgree}    //div[@class='mat-checkbox-frame']
${Finish}         //span[normalize-space()='Finish']
 ### Add Company ###
${addKnowledge}    //a[normalize-space()='Add knowledge']
${akVideo}        //button[@mattooltip='Add video']
${adImages}       //mat-icon[normalize-space()='image']
${adAudio}        //button[@mattooltip='Add audio']
${adText}         //button[@mattooltip='Add text']
${file_path_variable}    robot/data/cover.jpg
 ### Ask ###
${lmAsk}          //a[normalize-space()='Ask']
 ### Courses ###
${lmCourses}      //a[normalize-space()='Courses']
 ### Add Courses ###
${lmAddCourses}    //a[normalize-space()='Add course']
 ### Change Permissions"
${InvitationText}    An invite has been sent to your new team member. After they confirm their account they will be visible in your organisation.
@{ROBOTS}         Admin    Viewer    Creator
 ### API Variables ###
# ${HOST}         https://dev.sideways.work/api
# ${HOST}         https://dev.sideways.work/api
${LOCAL}          https://dev.sideways.work/login

*** Keywords ***
# robocop: disable
Set Suitup
    Global Variables
    Global API Variables
    Set Selenium Implicit Wait    5
    Set Selenium Timeout    5
    Capture Page Screenshot    EMBED
    Set Screenshot Directory    EMBED

Global Variables
    ${firstNameData}    FakerLibrary.First Name
    ${lastNameData}    FakerLibrary.Last Name
    ${randomNumb}    FakerLibrary.Random Number    4    fix_len=True
    ${userNameData}    Set Variable    ${firstNameData}${lastNameData}
    ${userNameDataRegister}    Set Variable    ${firstNameData}${lastNameData}${randomNumb}
    ${newEmail}    Set Variable    milan.novovic+${randomNumb}@equaleyes.com
    ${knowledgeTitle}    FakerLibrary.Sentence    4
    ${newTempEmail}    Set Variable    ${userNameData}@givmail.com
    Set Global Variable    ${knowledgeTitle}
    Set Global Variable    ${firstNameData}
    Set Global Variable    ${lastNameData}
    Set Global Variable    ${newEmail}
    Set Global Variable    ${userNameData}
    Set Global Variable    ${newTempEmail}
    Set Global Variable    ${userNameDataRegister}

Open Sideways Application
    ${chrome_options}    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}    add_argument    no-sandbox
    Call Method    ${chrome_options}    add_argument    headless
    Call Method    ${chrome_options}    add_argument    disable-gpu
    Call Method    ${chrome_options}    add_argument    disable-dev-shm-usage
    Call Method    ${chrome_options}    add_argument    disable-gpu
    ${options}    Call Method    ${chrome_options}    to_capabilities
    Open Browser    ${HOST}    ${BROWSER}    remote_url=${GRID_URL}
    Set Window Size    1920    1080
    #remote_url=${GRID_URL}

Sign In With Valid Email
    Click Element    ${username}
    Input Text    ${username}    milan.novovic@equaleyes.com
    Click Element    ${SignIn}
    Input Text    ${password}    Testingqa123
    Click Element    ${SignIn}

Enter valid email
    [Arguments]    ${validUsername}
    Click Element    ${username}
    Input Text    ${username}    ${validUsername}

Enter Valid Password
    [Arguments]    ${validPassword}
    Click Element    ${password}
    Input Text    ${password}    ${validPassword}

Click On Sign In Button
    Click Element    ${SignIn}

Home Page Will Be Presented
    Sleep    3
    Wait Until Element Is Enabled    (//img[@id='profileAvatarImg'])[1]
    Click Element    (//img[@id='profileAvatarImg'])[1]
    Wait Until Element Is Enabled    //*[@id='profileMenuProfile']
    Click Element    //*[@id='profileMenuProfile']

Click On Log Out Button
    Wait Until Element Is Enabled    (//img[@id='profileAvatarImg'])[1]
    Click Element    (//img[@id='profileAvatarImg'])[1]
    Wait Until Element Is Enabled    //*[@id='profileMenuLogOut']
    Click Element    //*[@id='profileMenuLogOut']

Sign In Button Should Be Present
    Page Should Contain Element    ${SignIn}

Click on Sign Up button
    Click Element    ${signUp}
    Maximize Browser Window
    Sleep    3s

Enter OrgName and SubDomain values
    ${orgNameText}    FakerLibrary.Company
    ${subDomainText}    FakerLibrary.Domain Name
    Input Text    ${orgName}    ${orgNameText}
    Input Text    ${subDomain}    ${subDomainText}
    Sleep    1s
    Click Element    //span[normalize-space()='Next step']

Wait Until Page Is Loaded
    Wait For Condition    return window.document.readyState === "complete"

Enter Valid Profile Registration Data
    Wait For Condition    return window.document.readyState === "complete"
    Wait Until Page Is Loaded
    Sleep    3
    Input Text    ${userName}    ${userNameDataRegister}
    Input Text    ${firstName}    ${firstNameData}
    Input Text    ${lastName}    ${lastNameData}
    Input Text    ${signEmail}    ${newTempEmail}
    Input Text    ${signPassword}    TestingQA123
    Input Text    ${repeatPassword}    TestingQA123

Click on T&C checkbox
    Page Should Contain Element    //*[@class='mat-checkbox-inner-container']
    Mouse Over    //*[@class='mat-checkbox-inner-container']
    Sleep    3s
    Click Element    //*[@class='mat-checkbox-inner-container']

Click on Finish Button
    Mouse Over    //*[contains(@class,'mat-button-wrapper') and contains(text(),' Finish ')]
    Sleep    3s
    Click Element    //*[contains(@class,'mat-button-wrapper') and contains(text(),' Finish ')]

Then Sign In button will appear
    Page Should Contain Element    //*[contains(@class,'mat-button-wrapper') and contains(text(),' Sign In')]
    Mouse Over    //*[contains(@class,'mat-button-wrapper') and contains(text(),' Sign In')]
    Click Element    //*[contains(@class,'mat-button-wrapper') and contains(text(),' Sign In')]

And Sideways Logo Will appear
    Wait Until Page Contains Element    ${SignIn}
    Page Should Contain Element    ${SignIn}

Click On Ask Page
    Wait Until Page Contains Element    //div[contains(@class,'mat-list-text')]//*[text() = 'Ask']
    Sleep    3
    Click Element    //div[contains(@class,'mat-list-text')]//*[text() = 'Ask']

Enters The Question
    Wait Until Page Contains Element    //*[@formcontrolname='questionText']    timeout=5s
    Input Text    //*[@formcontrolname='questionText']    What is Sideways?
    Click Element    //span[normalize-space()='Ask']
    Wait Until Element Is Enabled    //*[@class='question-text mt-1']    timeout=5s
    Sleep    3s
    ${value}    Get Text    //*[@class='question-text mt-1']
    Set Global Variable    ${value}
    Input Text    //*[@formcontrolname='questionText']    ${value}
    Click Element    //span[normalize-space()='Ask']

User Can Delete Question
    Mouse Over    //mat-list-item[1]//div[1]//div[3]//div[2]//div[1]//div[3]//button[1]
    Sleep    2
    Wait Until Element Is Enabled    //mat-list-item[1]//div[1]//div[3]//div[2]//div[1]//div[3]//button[1]    timeout=5s
    Click Element    //mat-list-item[1]//div[1]//div[3]//div[2]//div[1]//div[3]//button[1]
    Wait Until Element Is Enabled    //*[@id='btnDelete']
    Click Element    //*[@id='btnDelete']
    Wait Until Element Is Enabled    //*[contains(text(),'${value}')]    timeout=5s
    Page Should Contain Element    //*[contains(text(),'${value}')]

Click on Add Knowledge
    Sleep    2
    Wait Until Page Contains Element    ${addKnowledge}
    Click Element    ${addKnowledge}
    Wait Until Page Contains Element    ${adImages}
    Click Element    ${adImages}

Upload Files to Drop zone
    File Should Exist    ${CURDIR}/../data/cover.jpg
    Page Should Contain Element    //app-file-select//input[@type='file']
    Choose File    //app-file-select//input[@type='file']    ${CURDIR}/../data/cover.jpg
    Sleep    5
    # Win Kill    Open

Wait Until Upload is Finished
    Wait Until Element Is Visible    //div[contains(text(),' Uploaded 100% ')]    15
    Page Should Contain Element    //div[contains(text(),' Uploaded 100% ')]
    Sleep    2

Click On Next button
    Click Element    //span[normalize-space()='Next step']

Enters Knowledge Title And Short Description
    Wait Until Element is Visible    //input[@formcontrolname='name']
    Input Text    //input[@formcontrolname='name']    Title of Tuttorial
    Wait Until Element is Visible    //*[@formcontrolname='description']
    Input Text    //*[@formcontrolname='description']    Cover of the album
    Sleep    2

User will be enabled to Publish its Media
    Click Element    //*[contains(@class,'mat-button-wrapper') and contains(text(),'Publish')]
    Wait Until Element is Visible    //div[@class='image-card']
    Page Should Contain Element    //div[@class='image-card']

User Is On Bank Page
    Sleep    2
    Wait Until Element is Visible    //div[contains(@class,'mat-list-text')]//*[text() = 'Bank']
    Click Element    //div[contains(@class,'mat-list-text')]//*[text() = 'Bank']

Click on Existing Media
    Wait Until Element is Visible    (//*[@class='mat-card-content'])[1]
    Click Element    (//*[@class='mat-card-content'])[1]

Delete Selected Media
    Sleep    1
    Wait Until Element is Visible    //mat-icon[normalize-space()='delete_outline']
    Click Element    //mat-icon[normalize-space()='delete_outline']
    Wait Until Element is Visible    //span[normalize-space()='Delete']
    Click Element    //span[normalize-space()='Delete']

User Is On Orginazitaion/Members Page
    Wait Until Element Is Enabled    //a[normalize-space()='Organisation']
    Click Element    //a[normalize-space()='Organisation']
    Sleep    1
    Click Element    //a[normalize-space()='Members']

And Enters Users Details And Invite
    Wait Until Element is Visible    //input[@data-placeholder='First name']
    Input Text    //input[@data-placeholder='First name']    ${firstNameData}
    Wait Until Element is Visible    //input[@data-placeholder='Last name']
    Input Text    //input[@data-placeholder='Last name']    ${lastNameData}
    Wait Until Element is Visible    //input[@data-placeholder='Email']
    Input Text    //input[@data-placeholder='Email']    ${newEmail}
    Wait Until Element is Visible    //span[normalize-space()='Invite']
    Click Element    //span[normalize-space()='Invite']
    Sleep    4s

Notification Of User Being Invited Has Been Displayed
    Wait Until Element is Visible    //div[@fxlayout='column']//div//p
    Element Should Be Visible    //div[@fxlayout='column']//div//p
    ${invitation_text}    Get Text    //div[@fxlayout='column']//div//p
    ${assertion_text}    Set Variable    An invite has been sent to your new team member. After they confirm their account they will be visible in your organisation.
    Should Be Equal As Strings    ${invitation_text}    ${assertion_text}

User Is on Add Course page
    Wait Until Element is Visible    //div[contains(@class,'mat-list-text')]//*[text() = 'Add course']
    Click Element    //div[contains(@class,'mat-list-text')]//*[text() = 'Add course']

User Pick Knowledge
    Wait Until Element is Visible    //button[@id='newCourseAddContent']
    Click Element    //button[@id='newCourseAddContent']
    Sleep    2
    Wait Until Element is Visible    (//*[@class='mat-card-content'])[1]
    Click Element    (//*[@class='mat-card-content'])[1]
    Wait Until Element is Visible    //*[contains(@class,'mat-button-wrapper') and contains(text(),' Add to course ')]
    Click Element    //*[contains(@class,'mat-button-wrapper') and contains(text(),' Add to course ')]

Creates Questionare
    Sleep    3
    Wait Until Element is Visible    //button[@id='newCourseAddQuiz']
    Sleep    3
    Click Element    //button[@id='newCourseAddQuiz']
    Sleep    3
    Input Text    //*[@placeholder='Questionnaire title']    Questionnaire title
    Wait Until Element is Visible    //input[@formcontrolname='question']
    Input Text    //input[@formcontrolname='question']    What Is Sideways?
    Wait Until Element is Visible    //*[@class='add-option']
    Click Element    //*[@class='add-option']
    Input Text    //textarea[@data-placeholder='Write an option']    Social Media platform?
    Click Element    //*[@class='add-option']
    Input Text    (//textarea[@data-placeholder='Write an option'])[2]    WebShop?
    Click Element    //*[@class='add-option']
    Input Text    (//textarea[@data-placeholder='Write an option'])[3]    Learning Platform?
    Mouse Over    (//*[contains(@id,'mat-checkbox')])[5]
    Click Element    (//*[contains(@id,'mat-checkbox')])[5]
    Wait Until Element is Visible    //*[@class='mat-button-wrapper' and text()='Save']
    Click Element    //*[@class='mat-button-wrapper' and text()='Save']

Creates Tasks
    Wait Until Element is Visible    //button[@id='newCourseAddTask']
    Click Element    //button[@id='newCourseAddTask']
    Wait Until Element is Visible    //input[@data-placeholder='Task title']
    Input Text    //input[@data-placeholder='Task title']    Task Title
    Wait Until Element is Visible    //*[@data-placeholder='Write a task']
    Input Text    //*[@data-placeholder='Write a task']    Write first task
    Wait Until Element is Visible    //*[contains(text(),'Add a task')]
    Click Element    //*[contains(text(),'Add a task')]
    Input Text    (//*[@data-placeholder='Write a task'])[2]    Write another task
    Wait Until Element is Visible    //*[@class='mat-button-wrapper' and text()='Save']
    Click Element    //*[@class='mat-button-wrapper' and text()='Save']
    Wait Until Element is Visible    //button[@id='newCourseOptionsNextStep']
    Sleep    1
    Click Element    //button[@id='newCourseOptionsNextStep']

Add Knowledge Title
    Wait Until Element is Enabled    //*[@formcontrolname='name']
    Input Text    //*[@formcontrolname='name']    ${knowledgeTitle}
    Wait Until Element is Enabled    //*[@formcontrolname='description']
    Input Text    //*[@formcontrolname='description']    ${knowledgeTitle}
    Wait Until Page Is Loaded
    Wait Until Element is Enabled    //*[@class='mat-button-wrapper' and text()='Publish']
    Sleep    1
    Click Element    //*[@class='mat-button-wrapper' and text()='Publish']

New Course Can Be Created
    Wait Until Page Is Loaded
    Sleep    3
    Element Should Be Visible    (//*[contains(text(),'${knowledgeTitle}')])[1]

Then Show Time and Date
    Get Current Date

Go to Courses Page
    Sleep    1
    Click Element    //div[contains(@class,'mat-list-text')]//*[text() = 'Courses']
    Sleep    3

Check And Delete If There Are Courses
    Sleep    3
    ${count}    Get Element Count    //*[@class='mat-card-content']
    Log    ${count}
    ${count}=    Evaluate    ${count} - 1
    Log    ${count}
    IF    ${count} == 0
    Pass Execution    All features available in this version tested.
    ELSE IF    ${count} > 0
    Repeat Keyword    ${count} times    And Delete One of the content
    END

Delete All Courses From Page
    @{elements}    Get Web Elements    (//*[@class='mat-card-content'])[1]
    FOR    ${element}    IN    @{elements}
        Sleep    2
        Click Element    (//*[@class='mat-card-content'])[1]
        Sleep    2
        ${result}=    Run keyword and ignore error    Element should be visible    (//mat-icon[normalize-space()='delete_outline'])[1]
        Run Keyword If
        ...    '${result[0]}' == 'PASS'
        ...    Delete Course
        ...    ELSE
        ...    Pass Execution    All features available in this version tested.
    END
    Log    All Courses have been deleted

And Delete One of the content
    Wait Until Page Contains Element    (//*[@class='mat-card-content'])[1]
    Click Element    (//*[@class='mat-card-content'])[1]
    Sleep    2
    ${result}    Run keyword and ignore error    Element should be visible    //mat-icon[normalize-space()='delete_outline']
    Run keyword if    '${result[0]}' == 'PASS'
    ...    Delete Course
    ...    ELSE
    ...    Log    There's no Courses to be deleted
    Sleep    2s

Delete Course
    Wait Until Element Is Enabled    //mat-icon[normalize-space()='delete_outline']
    Click Element    //mat-icon[normalize-space()='delete_outline']
    Wait Until Element Is Enabled    //*[normalize-space()='Delete']
    Click Element    //*[normalize-space()='Delete']
    Sleep    2

Delete Content From Bank
    Wait Until Element is Enabled    //div[contains(@class,'mat-list-text')]//*[text() = 'Bank']    timeout=5s
    Click Element    //div[contains(@class,'mat-list-text')]//*[text() = 'Bank']
    Sleep    2
    @{elements}    Get Web Elements    //*[@class='mat-card-content']
    FOR    ${element}    IN    @{elements}
        Sleep    3
        Click Element    //*[@class='mat-card-content']
        ${result}    Run keyword and ignore error    Element should be visible    //mat-icon[normalize-space()='delete_outline']
        Log    ${result}
        Run Keyword If
        ...    '${result[0]}' == 'PASS'
        ...    Delete Single Content
        ...    ELSE
        ...    Exit For Loop
    END
    Log    No More Data in the Bank

Delete Single Content
    Wait Until Element Is Enabled    //mat-icon[normalize-space()='delete_outline']    timeout=5s
    Click Element    //mat-icon[normalize-space()='delete_outline']
    Sleep    1
    Click Element    //span[normalize-space()='Delete']
    Sleep    1
    ${result}    Run keyword and ignore error    Element should be visible    (//button[contains(@class,'mat-focus-indicator')])[9]
    Pass Execution If    '${result[0]}' == 'PASS'    All Content has been Deleted

Notification About Email Will Be Presented
    Wait Until Element Is Enabled    //*[contains(text(),'The verification link will expire in 3 days')]
    Element Should Be Visible    //*[contains(text(),'The verification link will expire in 3 days')]
    ${invitation_text}    Get Text    //*[contains(text(),'The verification link will expire in 3 days')]
    ${assertion_text}    Set Variable    The verification link will expire in 3 days
    Should Be Equal As Strings    ${invitation_text}    ${assertion_text}

Go To Organization/Teams Page
    Wait Until Element is Enabled    id=organisationSideNavButton
    Click Element    id=organisationSideNavButton
    Wait Until Element is Enabled    //a[text() = 'Teams']
    Click Element    //a[text() = 'Teams']

Enter Team Name And Create
    Sleep    2
    Wait Until Element is Visible    //*[@formcontrolname='teamName']
    Input Text    //*[@formcontrolname='teamName']    ${userNameData}
    Wait Until Element is Visible    //span[.=' Create ']
    Click Element    //span[.=' Create ']
    Sleep    2

Click On Created Team
    Click Element    //div[text() = '${userNameData}']
    Sleep    1
    Wait Until Element is Visible    //mat-icon[normalize-space()='person_add']

Click On First Name From List
    Click Element    //mat-icon[normalize-space()='person_add']
    Wait Until Element is Enabled    (//*[contains(@role,'i')]//*[text() = ' add_circle_outline '])[1]
    Click Element    (//*[contains(@role,'i')]//*[text() = ' add_circle_outline '])[1]

Assign Person To Team
    Sleep    2
    Wait Until Element is Enabled    //span[text() = 'Save']
    Click Element    //span[text() = 'Save']
    Sleep    1

Page Should Display That Member Was Added
    Page Should Contain Element    //span[text() = 'Members have been added to your team']

Go To Organization/Members Page
    Wait Until Element is Enabled    id=organisationSideNavButton
    Click Element    id=organisationSideNavButton
    Wait Until Element is Enabled    //a[text() = 'Members']
    Click Element    //a[text() = 'Members']
    Wait Until Element is Visible    //h2[normalize-space()='New member']

Enter User Details
    ${firstNameData}    FakerLibrary.First Name
    ${lastNameData}    FakerLibrary.Last Name
    ${newTempEmailz}    Set Variable    ${firstNameData}${LastNameData}@givmail.com
    Wait Until Element Is Visible    //input[@data-placeholder='First name']
    Input Text    //input[@data-placeholder='First name']    ${firstNameData}
    Wait Until Element Is Visible    //input[@data-placeholder='Last name']
    Input Text    //input[@data-placeholder='Last name']    ${LastNameData}
    Wait Until Element Is Visible    //input[@data-placeholder='Email']
    Input Text    //input[@data-placeholder='Email']    ${newTempEmailz}
    Wait Until Element Is Visible    //*[@formcontrolname='permissionRole']
    Click Element    //*[@formcontrolname='permissionRole']
    Wait Until Element Is Visible    (//span[normalize-space()='${robot}'])[1]
    Click Element    (//span[normalize-space()='${robot}'])[1]
    Wait Until Element Is Visible    //span[normalize-space()='Invite']
    Click Element    //span[normalize-space()='Invite']
    Wait Until Element Is Visible    //*[text() = " ${InvitationText} "]
    Element Should Be Visible    //*[text() = " ${InvitationText} "]
    Sleep    2
    Wait Until Element Is Visible    //span[normalize-space()='Close']
    Click Element    //span[normalize-space()='Close']

Loop Through Permissions
    FOR    ${robot}    IN    @{ROBOTS}
        Set Global Variable    ${robot}
        Log    ${robot}
        Sleep    1
        Run keyword    Enter User Details
    END

Go To Organization/Location Page
    Sleep    2
    Wait Until Element Is Enabled    id=organisationSideNavButton
    Click Element    id=organisationSideNavButton
    Wait Until Element is Enabled    //a[text() = 'Location']
    Click Element    //a[text() = 'Location']
    Wait Until Element is Visible    //h2[normalize-space()='New location']

Create Area and Repeat Process for Number of Times
    [Arguments]    ${numRepetetion}
    Repeat Keyword    ${numRepetetion} times    Check if Address is Created

Check if Address is Created
    ${area}    FakerLibrary.City
    Set Global Variable    ${area}
    Sleep    1
    Log    ${area}
    Input Text    //input[@formcontrolname='areaName']    ${area}
    Wait Until Element is Enabled    //*[contains(@class,'mat-button-wrapper') and contains(text(),'Create')]
    Click Element    //*[contains(@class,'mat-button-wrapper') and contains(text(),'Create')]
    Reload Page
    Sleep    2
    Page Should Contain Element    //h2[text() = '${area}']

Check Number Of Areas And Delete All Areas
    @{elements}    Get Web Elements    //*[@class='mb-0']
    FOR    ${element}    IN    @{elements}
        Sleep    1
        Click Element    (//*[@class='mat-button-wrapper'])[7]
        Wait Until Element is Enabled    (//*[contains(@class,'mat-button-wrapper') and contains(text(),'Remove whole area')])[1]
        Click Element    (//*[contains(@class,'mat-button-wrapper') and contains(text(),'Remove whole area')])[1]
        Wait Until Element is Enabled    (//span[contains(@class,'mat-button-wrapper') and contains(text(),' Delete ')])[1]
        Click Element    (//span[contains(@class,'mat-button-wrapper') and contains(text(),' Delete ')])[1]
        Sleep    1
    END
    Page Should Contain Element    //h3[text() = ' You can create it above ']

Create Area With Location
    ${address}    FakerLibrary.Street Address
    Check if Address is Created
    Wait Until Element is Enabled    //input[@formcontrolname='location']
    Input Text    //input[@formcontrolname='location']    ${address}
    Wait Until Element is Enabled    //mat-select[@formcontrolname='areaSelect']
    Click Element    //mat-select[@formcontrolname='areaSelect']
    Wait Until Element is Enabled    //span[contains(@class,'mat-option-text') and contains(text(),'${area}')]
    Click Element    //span[contains(@class,'mat-option-text') and contains(text(),'${area}')]
    Wait Until Element is Enabled    //*[contains(@class,'mat-button-wrapper') and contains(text(),' Add')]
    Click Element    //*[contains(@class,'mat-button-wrapper') and contains(text(),' Add')]
    Wait Until Element is Enabled    //div[normalize-space()='${address}']
    Page Should Contain Element    //div[normalize-space()='${address}']

Create Area With Location Process For Number Of Times
    [Arguments]    ${numRepetetion}
    Repeat Keyword    ${numRepetetion} times    Create Area With Location

Check If there are Questions And Delete If Any
    Sleep    3
    ${result}    Run keyword and ignore error    Element should be visible    //h3[normalize-space()='Be the the first one to do it']
    Log    ${result[0]}
    Run keyword if
    ...    '${result[0]}' == 'PASS'
    ...    Log    There's no Questions to be deleted
    ...    ELSE
    ...    Delete All Questions from Page

Delete All Questions from Page
    @{elements}    Get Web Elements    //div[normalize-space()='more_horiz_24px']
    FOR    ${element}    IN    @{elements}
        Sleep    1
        Click Element    //div[normalize-space()='more_horiz_24px']
        Sleep    2
        Click Element    //button[normalize-space()='Delete question']
        Sleep    2
        Reload Page
    END

Check If There Are Teams Created And Delete If Any
    Sleep    3
    ${result}    Run keyword and ignore error    Element should be visible    //h3[normalize-space()='You can create them above']
    Log    ${result[0]}
    Run keyword if
    ...    '${result[0]}' == 'PASS'
    ...    Log    There's no Teams to be deleted
    ...    ELSE
    ...    Select and Delete All Teams

Select and Delete All Teams
    @{elements}    Get Web Elements    //div[@class='mr-3']
    FOR    ${element}    IN    @{elements}
        Sleep    1
        Wait Until Element is Enabled    //mat-icon[normalize-space()='delete_outline']
        Click Element    //mat-icon[normalize-space()='delete_outline']
        Sleep    2
        Click Element    (//*[contains(@class,'mat-pseudo-checkbox ng-star-inserted')])[1]
        Sleep    2
        Click Element    //span[normalize-space()='Remove']
        Sleep    2
        Click Element    //span[normalize-space()='Delete']
        Sleep    2
        Reload Page
        Sleep    2
    END
    Page Should Contain Element    //h3[normalize-space()='You can create them above']

Check If There Are Memebers And Delete All
    Click Element    //mat-icon[normalize-space()='delete_outline']
    @{elements}    Get Web Elements    (//mat-pseudo-checkbox[@class='mat-pseudo-checkbox ng-star-inserted'])
    FOR    ${element}    IN    @{elements}
        Sleep    1
        Wait Until Element is Enabled    //mat-icon[normalize-space()='delete_outline']
        Click Element    //mat-icon[normalize-space()='delete_outline']
        Wait Until Element is Enabled    (//*[contains(@class,'mat-pseudo-checkbox ng-star-inserted')])[1]
        Click Element    (//*[contains(@class,'mat-pseudo-checkbox ng-star-inserted')])[1]
        Wait Until Element is Enabled    //span[normalize-space()='Remove']
        Click Element    //span[normalize-space()='Remove']
        Sleep    2
        Click Element    //span[normalize-space()='Delete']
        Sleep    2
        Reload Page
        Sleep    2
    END
