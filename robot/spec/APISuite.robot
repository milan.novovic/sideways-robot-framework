*** Settings ***
Suite Setup       Set Suitup
Suite Teardown    Close Browser
Test Teardown     Close Browser
Library           String
Library           Collections
Library           OperatingSystem
Library           JSONLibrary
Library           SeleniumLibrary    timeout=7 seconds
Library           FakerLibrary
Library           ExampleLibrary.py
Resource          ../libs/keyword_WEB_resource.robot
Resource          ../libs/keyword_API_resource.robot

*** Test Cases ***
TC-12 API - Validate User Login Create
    [Tags]    api    usercreate
    Create Session    USER_LOGIN    ${HOST}/api    verify=True
    And User Send Login POST Request
    Response Status Is 200 OK
    [Teardown]    NONE

TC-13 API - Validate User Registration
    [Tags]    api    registration
    Create Session    USER_REGISTRATION    ${HOST}/api    verify=True
    User Send Registration POST Request
    Response Status is 201 OK
    Create Session    GETNADA_EMAIL    ${HOST_GETNADA}    verify=True
    User Gets Email Content
    Create Session    GETNADA_MESSAGE    ${HOST_GETNADA_MESSAGE}    verify=True
    User Gets Registration Link
    Then Is Possible To Login With Created Email

TC-14 API - POST Text via API method
    [Tags]    api    text
    Create Session    USER_LOGIN    ${HOST}/api    verify=True
    And User Send Login POST Request
    Create Session    CREATE_CONTENT    ${HOST}/api    verify=True
    Then User Can Create Content
    Create Session    UPLOAD_TEXT    ${HOST}/api    verify=True
    Then User Can Upload Text
    [Teardown]    NONE

TC-15 API - POST Image via API method
    [Tags]    api    postimage
    Create Session    USER_LOGIN    ${HOST}/api    verify=True
    And User Send Login POST Request
    Create Session    CREATE_CONTENT    ${HOST}/api    verify=True
    Then User Can Create Content
    Create Session    UPLOAD_IMAGE    ${HOST}/api    verify=True
    Then User Can Upload Image
    [Teardown]    NONE

TC-16 API - DELETE Content via API method
    [Tags]    api    deleteimage
    Create Session    USER_LOGIN    ${HOST}/api    verify=True
    And User Send Login POST Request
    Create Session    DELETE_IMAGE    ${HOST}/api    verify=True
    Then User Can Delete Image
    [Teardown]    NONE

TC-17 API Delete ALL content via API
    [Tags]    api
    Create Session    USER_LOGIN    ${HOST}/api    verify=True
    User Send Login POST Request
    Create Session    GET_CONTENT    ${HOST}/api    verify=True
    Get Content List And Delete All Content
    [Teardown]    NONE
