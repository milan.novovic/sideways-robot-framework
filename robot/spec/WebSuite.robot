*** Settings ***
Suite Setup       Set Suitup
Suite Teardown    Close Browser
Test Teardown     Close Browser
Library           OperatingSystem
Library           Collections
Library           String
Library           JSONLibrary
Library           SeleniumLibrary    timeout=7 seconds
Library           FakerLibrary
Library           ExampleLibrary.py
Resource          ../libs/keyword_WEB_resource.robot
Resource          ../libs/keyword_API_resource.robot

*** Test Cases ***
TC-1 Validate Log In To Application flow
    [Tags]    smoke
    Open Sideways Application
    Enter valid email    ${validUsername}
    And Enter Valid Password    ${validPassword}
    And Click On Sign In Button
    Then Home page will be presented

TC-2 Validate Log Out from the Account flow
    [Tags]    smoke
    Open Sideways Application
    Enter valid email    ${validUsername}
    And enter valid password    ${validPassword}
    And Click on Sign In button
    And Click On Log Out Button
    Then Sign In Button Should Be Present

TC-3 Validate Sign Up with Valid Email Address flow
    [Tags]    smoke
    Open Sideways Application
    Click on Sign Up button
    #    Enter OrgName and SubDomain values
    Enter Valid Profile Registration Data
    Click on T&C checkbox
    Click on Finish Button
    Then Notification About Email Will Be Presented
    #    Then Sign In button will appear

TC-4 Validate Ask and Delete option flow
    [Tags]    smoke
    Open Sideways Application
    Sign In With Valid Email
    Then Home Page Will Be Presented
    And Click On Ask Page
    And Enters The Question
    Then User Can Delete Question
    Check If there are Questions And Delete If Any

TC-5 Validate Add Knowledge section can upload files
    [Tags]    smoke
    Open Sideways Application
    Sign In With Valid Email
    Then Home page will be presented
    Click on Add Knowledge
    Upload Files to Drop zone
    # Wait Until Upload is Finished
    And Click On Next Button
    And Enters Knowledge Title And Short Description
    Then User will be enabled to Publish its Media

TC-6 Validate that User can Delete Course from Bank
    [Tags]    smoke
    Open Sideways Application
    Sign In With Valid Email
    Then Home page will be presented
    Then Show Time and Date
    And User Is On Bank Page
    And Click on Existing Media
    And Delete Selected Media

TC-7 Validate Add New Members flow
    [Tags]    smoke
    Open Sideways Application
    Sign In With Valid Email
    And User Is On Orginazitaion/Members page
    And And Enters Users Details And Invite
    Then Notification Of User Being Invited Has Been Displayed

TC-8 Validate Add Course flow
    [Tags]    smoke
    Open Sideways Application
    Sign In With Valid Email
    And User Is on Add Course page
    And User Pick Knowledge
    And Creates Questionare
    And Creates Tasks
    And Add Knowledge Title
    Then New Course Can Be Created

TC-9 Delete Content From Course
    [Tags]    smoke
    Open Sideways Application
    Sign In With Valid Email
    Go to Courses Page
    Then Check And Delete If There Are Courses
    # Delete All Courses from page

TC-10 Delete All Content From Bank
    [Tags]    smoke
    Open Sideways Application
    Sign In With Valid Email
    Delete Content From Bank

TC-11 Create New Team
    Open Sideways Application
    Sign In With Valid Email
    Go To Organization/Teams Page
    Enter Team Name And Create
    Click On Created Team
    Click On First Name From List
    Assign Person To Team
    # Page Should Display That Member Was Added

TC-12 Set Users Role
    [Tags]    regression
    Open Sideways Application
    Sign In With Valid Email
    Go To Organization/Members Page
    Loop Through Permissions

TC-13 Create New Area
    Open Sideways Application
    Sign In With Valid Email
    Go To Organization/Location Page
    Create Area and Repeat Process for Number of Times    5

TC-14 Delete Areas Created
    [Tags]    regression
    Open Sideways Application
    Sign In With Valid Email
    Go To Organization/Location Page
    Check Number Of Areas And Delete All Areas
    # [Teardown]    Run keyword if    '${TEST STATUS}' == 'PASS'    Log    ${TEST STATUS}

TC-15 Add New Area and Location
    [Tags]    regression
    Open Sideways Application
    Sign In With Valid Email
    Go To Organization/Location Page
    Create Area With Location Process For Number Of Times    5
    Check Number Of Areas And Delete All Areas

TC-16 Delete All Teams
    [Tags]    regression
    Open Sideways Application
    Sign In With Valid Email
    Go To Organization/Teams Page
    Check If There Are Teams Created And Delete If Any

TC-17 Remove All Members
    [Tags]    regression
    Open Sideways Application
    Sign In With Valid Email
    Go To Organization/Members Page
    Check If There Are Memebers And Delete All
